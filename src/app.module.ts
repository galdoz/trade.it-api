import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { validationSchema } from './config';
import { UsersModule } from './modules/users/users.module';
import { AuthenticationModule } from './modules/authentication/authentication.module';
import { AuthorizationModule } from './modules/authorization/authorization.module';
import { ItemsModule } from './modules/items/items.module';
import { InventoryModule } from './modules/inventory/inventory.module';
import { TradeOffersModule } from './modules/trade-offers/trade-offers.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema,
      expandVariables: true,
    }),
    AuthenticationModule,
    UsersModule,
    AuthorizationModule,
    ItemsModule,
    InventoryModule,
    TradeOffersModule,
  ],
  controllers: [],
  providers: [],
  exports: [],
})
export class AppModule {}
