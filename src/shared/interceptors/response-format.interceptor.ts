import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  HttpException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

export class ResponseFormat<T> {
  message: string;
  data: T;
}

@Injectable()
export class ResponseFormatInterceptor<T> implements NestInterceptor<T, any> {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map((data) => ({
        message: data.message || 'success',
        data: data.data,
      })),
      catchError((error) => {
        if (error instanceof HttpException) {
          const response = error.getResponse() as Record<string, any>;
          const message = response.message || 'An error occurred';
          const code = response.code || 'UNKNOWN_ERROR';
          throw new HttpException(
            {
              message,
              code,
              data: null,
            },
            error.getStatus(),
          );
        } else {
          throw new HttpException(
            {
              message: 'An unexpected error occurred',
              code: 'UNKNOWN_ERROR',
              data: null,
            },
            500,
          );
        }
      }),
    );
  }
}
