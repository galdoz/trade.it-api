import { DocumentBuilder } from '@nestjs/swagger';

export const swaggerConfig = new DocumentBuilder()
  .setTitle('Trade.it API')
  .setDescription('API for Trade.it')
  .setVersion(process.env.npm_package_version || '1.0.0')
  .addServer(
    process.env.API_SERVER_URL || 'http://localhost:3000',
    'API Server URL',
  )
  .addBearerAuth()
  .build();
