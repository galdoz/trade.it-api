import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { AccessTokenGuard } from '../authorization/guards/access-token.guard';
import { InventoryService } from './inventory.service';
import { DeleteItemDto, TransferItemDto } from './dto';
import { UsersService } from '../users/users.service';
import { ItemsService } from '../items/items.service';
import { ResponseFormat } from '../../shared/interceptors/response-format.interceptor';

@Controller('inventory')
@ApiBearerAuth()
@UseGuards(AccessTokenGuard)
@ApiTags('Inventory')
export class InventoryController {
  constructor(
    private readonly inventoryService: InventoryService,
    private readonly usersService: UsersService,
    private readonly itemsService: ItemsService,
  ) {}

  @Get(':user_uuid')
  @ApiOperation({
    summary: "Get user's inventory",
    description: "Get user's inventory",
  })
  @ApiParam({
    name: 'user_uuid',
    type: 'string',
    description: "User's uuid",
  })
  async getInventory(
    @Param('user_uuid', ParseUUIDPipe) user_uuid: string,
  ): Promise<ResponseFormat<unknown>> {
    const inventory = await this.inventoryService.findMany({
      where: {
        users: {
          uuid: user_uuid,
        },
      },
    });

    if (inventory.user_items.length === 0) {
      throw new NotFoundException({
        message: 'User has no items in inventory',
        code: 'USER_HAS_NO_ITEMS_IN_INVENTORY',
      });
    }

    return {
      message: 'User inventory retrieved successfully',
      data: {
        total: inventory.count,
        items: inventory.user_items.map((item) => item.items),
      },
    };
  }

  @Post(':user_uuid/transfer')
  @ApiOperation({
    summary: "Transfer item to a specific user's inventory",
    description: 'Transfer item to a specific user inventory',
  })
  @ApiParam({
    name: 'user_uuid',
    type: 'string',
    description: "User's uuid",
  })
  async transferItem(
    @Param('user_uuid', ParseUUIDPipe) user_uuid: string,
    @Body() body: TransferItemDto,
  ): Promise<ResponseFormat<unknown>> {
    const { item_uuid } = body;
    const user = await this.usersService.findOne({
      uuid: user_uuid,
    });
    if (!user) {
      throw new BadRequestException('User not found');
    }
    const item = await this.itemsService.findOne({
      uuid: item_uuid,
    });
    if (!item) {
      throw new BadRequestException('Item not found');
    }
    const result = this.inventoryService.transferItem({
      itemId: item.id,
      userId: user.id,
    });

    return {
      message: 'Item transferred successfully',
      data: result,
    };
  }

  @Delete(':user_uuid/item')
  @ApiOperation({
    summary: "Delete a specific item of a user's inventory",
    description: "Delete a specific item of a user's inventory",
  })
  @ApiParam({
    name: 'user_uuid',
    type: 'string',
    description: "User's uuid",
  })
  async deleteUserItem(
    @Param('user_uuid', ParseUUIDPipe) user_uuid: string,
    @Body() body: DeleteItemDto,
  ): Promise<ResponseFormat<unknown>> {
    const { item_uuid } = body;
    const user = await this.usersService.findOne({
      uuid: user_uuid,
    });

    if (!user) {
      throw new BadRequestException('User not found');
    }

    const item = await this.itemsService.findOne({
      uuid: item_uuid,
    });

    if (!item) {
      throw new BadRequestException('Item not found');
    }

    const inventory = await this.inventoryService.findOne({
      item_id: item.id,
      user_id: user.id,
    });

    if (!inventory) {
      throw new BadRequestException("Item not found in user's inventory");
    }

    const result = this.inventoryService.delete({
      id: inventory.id,
    });

    return {
      message: 'Item deleted successfully',
      data: result,
    };
  }
}
