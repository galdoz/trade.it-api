import { ApiProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';

export class TransferItemDto {
  @ApiProperty({
    description: 'Item uuid that will be transferred',
    example: 'd0f0a4e0-4b5a-4e5a-8b3a-2b1b9e0d0b1e',
  })
  @IsUUID()
  item_uuid: string;
}
