import { Module } from '@nestjs/common';
import { PrismaService } from '../../shared/services/prisma.service';
import { InventoryService } from './inventory.service';
import { InventoryController } from './inventory.controller';
import { UsersModule } from '../users/users.module';
import { ItemsModule } from '../items/items.module';

@Module({
  providers: [PrismaService, InventoryService],
  controllers: [InventoryController],
  imports: [UsersModule, ItemsModule],
  exports: [InventoryService],
})
export class InventoryModule {}
