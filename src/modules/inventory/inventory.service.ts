import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../shared/services/prisma.service';
import { Prisma, user_items } from '@prisma/client';

@Injectable()
export class InventoryService {
  constructor(private readonly prisma: PrismaService) {}

  async create(data: Prisma.user_itemsCreateInput): Promise<user_items> {
    return this.prisma.user_items.create({ data });
  }

  async findOne(
    user_itemsWhereUniqueInput: Prisma.user_itemsWhereUniqueInput,
  ): Promise<user_items | null> {
    return this.prisma.user_items.findUnique({
      where: user_itemsWhereUniqueInput,
    });
  }

  async findMany(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.user_itemsWhereUniqueInput;
    where?: Prisma.user_itemsWhereInput;
    orderBy?: Prisma.user_itemsOrderByWithAggregationInput;
  }): Promise<{ count: number; user_items: any[] }> {
    const { skip, take, cursor, where, orderBy } = params;
    const [count, user_items] = await this.prisma.$transaction([
      this.prisma.user_items.count(),
      this.prisma.user_items.findMany({
        skip,
        take,
        cursor,
        where,
        orderBy,
        include: {
          items: true,
        },
      }),
    ]);
    return {
      count,
      user_items,
    };
  }

  async update(params: {
    where: Prisma.user_itemsWhereUniqueInput;
    data: Prisma.user_itemsUpdateInput;
  }): Promise<user_items> {
    const { where, data } = params;
    return this.prisma.user_items.update({ data, where });
  }

  async delete(where: Prisma.user_itemsWhereUniqueInput): Promise<user_items> {
    return this.prisma.user_items.delete({ where });
  }

  async transferItem(params: { itemId: number; userId: number }) {
    const { itemId, userId } = params;

    return this.prisma.user_items.upsert({
      update: {
        user_id: userId,
        item_id: itemId,
        updated_at: new Date(),
      },
      create: {
        user_id: userId,
        item_id: itemId,
        created_at: new Date(),
      },
      where: {
        item_id: itemId,
      },
    });
  }
}
