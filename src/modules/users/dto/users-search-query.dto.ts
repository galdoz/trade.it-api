import { ApiProperty } from '@nestjs/swagger';
import { Prisma } from '@prisma/client';
import { IsEnum, IsOptional } from 'class-validator';
import { SearchQueryDto } from '../../../shared/dto/search-query.dto';

export class UsersSearchQueryDto extends SearchQueryDto {
  @ApiProperty({
    enum: Prisma.UsersScalarFieldEnum,
    description: 'Sort by field',
    example: 'id',
    required: false,
  })
  @IsOptional()
  @IsEnum(Prisma.UsersScalarFieldEnum, { each: true })
  orderBy?: string;
}
