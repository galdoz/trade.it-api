import { ApiProperty } from '@nestjs/swagger';
import { users } from '@prisma/client';
import { IsEmail, IsString, MinLength } from 'class-validator';

export class NewUserDto
  implements Pick<users, 'email' | 'first_name' | 'last_name'>
{
  @ApiProperty({
    example: 'my-amazing-email@email.com',
    description: 'The email of the User',
  })
  @IsEmail({})
  email: string;

  @ApiProperty({
    example: 'John',
    description: 'The first name of the User',
    minLength: 4,
  })
  @MinLength(4)
  @IsString({})
  first_name: string;

  @ApiProperty({
    example: 'Doe',
    description: 'The last name of the User',
    minLength: 3,
  })
  @IsString()
  @MinLength(3)
  last_name: string;

  @ApiProperty({
    example: 'MyAmazingPassword',
    description: 'The password of the User',
    minLength: 4,
  })
  @IsString()
  @MinLength(4)
  passsword: string;
}
