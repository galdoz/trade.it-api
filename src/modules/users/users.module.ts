import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { PrismaService } from '../../shared/services/prisma.service';
import { EncrypterSevice } from '../../shared/services/encrypter.service';

@Module({
  controllers: [UsersController],
  providers: [UsersService, PrismaService, EncrypterSevice],
  exports: [UsersService],
})
export class UsersModule {}
