import { ApiProperty } from '@nestjs/swagger';
import { SearchQueryDto } from '../../../shared/dto/search-query.dto';
import { Prisma } from '@prisma/client';
import { IsEnum, IsOptional } from 'class-validator';

export class TradeOffersSearchDto extends SearchQueryDto {
  @ApiProperty({
    enum: Prisma.Trade_offersScalarFieldEnum,
    description: 'Sort by field',
    example: 'id',
    required: false,
  })
  @IsOptional()
  @IsEnum(Prisma.Trade_offersScalarFieldEnum, { each: true })
  orderBy?: string;
}
