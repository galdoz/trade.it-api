import { ApiProperty } from '@nestjs/swagger';
import { ArrayMinSize, IsArray, IsUUID } from 'class-validator';

export class NewTradeofferDto {
  @ApiProperty({
    description: 'The uuid of the receiving user',
    type: 'string',
    format: 'uuid',
    example: 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
  })
  @IsUUID()
  receiving_user_uuid: string;

  @IsArray({})
  @ArrayMinSize(1)
  @IsUUID('4', { each: true })
  @ApiProperty({
    description: 'Uuids of the items that you want to send to the other user',
    type: 'array',
    items: {
      type: 'string',
      format: 'uuid',
    },
    example: ['a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11'],
  })
  sending_user_items: string[];

  @IsArray({})
  @ArrayMinSize(1)
  @IsUUID('4', { each: true })
  @ApiProperty({
    description:
      'Uuids of the items that you want to receive from the other user',
    type: 'array',
    items: {
      type: 'string',
      format: 'uuid',
    },
    example: ['a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11'],
  })
  receiving_user_items: string[];
}
