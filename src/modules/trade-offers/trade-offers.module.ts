import { Module } from '@nestjs/common';
import { TradeOffersService } from './trade-offers.service';
import { TradeOffersController } from './trade-offers.controller';
import { PrismaService } from '../../shared/services/prisma.service';
import { ItemsModule } from '../items/items.module';
import { UsersModule } from '../users/users.module';
import { InventoryModule } from '../inventory/inventory.module';

@Module({
  providers: [TradeOffersService, PrismaService],
  controllers: [TradeOffersController],
  imports: [ItemsModule, UsersModule, InventoryModule],
})
export class TradeOffersModule {}
