import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../shared/services/prisma.service';
import { Prisma, trade_offers } from '@prisma/client';

@Injectable()
export class TradeOffersService {
  constructor(private readonly prisma: PrismaService) {}

  async create(data: Prisma.trade_offersCreateInput): Promise<trade_offers> {
    return this.prisma.trade_offers.create({ data });
  }

  async delete(
    where: Prisma.trade_offersWhereUniqueInput,
  ): Promise<trade_offers> {
    return this.prisma.trade_offers.delete({ where });
  }

  async update(params: {
    where: Prisma.trade_offersWhereUniqueInput;
    data: Prisma.trade_offersUpdateInput;
  }): Promise<trade_offers> {
    const { where, data } = params;
    return this.prisma.trade_offers.update({ data, where });
  }

  async findOne(
    where: Prisma.trade_offersWhereUniqueInput,
  ): Promise<trade_offers | null> {
    return this.prisma.trade_offers.findUnique({ where });
  }

  async findMany(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.trade_offersWhereUniqueInput;
    where?: Prisma.trade_offersWhereInput;
    orderBy?: Prisma.trade_offersOrderByWithAggregationInput;
  }): Promise<{ total: number; trade_offers: trade_offers[] }> {
    const { skip, take, cursor, where, orderBy } = params;

    const [total, trade_offers] = await this.prisma.$transaction([
      this.prisma.trade_offers.count(),
      this.prisma.trade_offers.findMany({
        skip,
        take,
        cursor,
        where,
        orderBy,
      }),
    ]);

    return {
      total,
      trade_offers,
    };
  }
}
