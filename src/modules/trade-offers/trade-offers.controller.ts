import {
  BadRequestException,
  Body,
  Controller,
  Get,
  NotFoundException,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AccessTokenGuard } from '../authorization/guards/access-token.guard';
import { NewTradeofferDto, TradeOffersSearchDto } from './dto';
import { TradeOffersService } from './trade-offers.service';
import { User } from '../../shared/decorators';
import { users } from '@prisma/client';
import { UsersService } from '../users/users.service';
import { InventoryService } from '../inventory/inventory.service';
import { TradeOfferStatus } from './constants';
import { ResponseFormat } from '../../shared/interceptors/response-format.interceptor';

@Controller('trade-offers')
@ApiBearerAuth()
@ApiTags('Trade Offers')
@UseGuards(AccessTokenGuard)
export class TradeOffersController {
  constructor(
    private readonly tradeOffersService: TradeOffersService,
    private readonly usersService: UsersService,
    private readonly inventoryService: InventoryService,
  ) {}
  @Get()
  @ApiOperation({
    summary: 'Get trade offers',
    description: 'Get trade offers',
  })
  async getTradeOffers(
    @Query() query: TradeOffersSearchDto,
  ): Promise<ResponseFormat<unknown>> {
    const { orderBy, skip, sortBy, take } = query;
    const result = await this.tradeOffersService.findMany({
      orderBy: { [orderBy]: sortBy },
      skip,
      take,
    });

    if (result.trade_offers.length <= 0) {
      throw new NotFoundException('No trade offers found');
    }

    return {
      message: 'All trade offers',
      data: result,
    };
  }

  @Post('')
  @ApiOperation({
    summary: 'Create a new trade offer',
    description:
      'The person who wants to send the exchange offer must be authenticated by the access token.',
  })
  async createTradeOffer(
    @Body() body: NewTradeofferDto,
    @User() user: users,
  ): Promise<ResponseFormat<unknown>> {
    const {} = user;
    const { receiving_user_items, receiving_user_uuid, sending_user_items } =
      body;
    const receivingUser = await this.usersService.findOne({
      uuid: receiving_user_uuid,
    });
    if (!receivingUser) {
      throw new BadRequestException({
        message: 'The receiving user does not exist',
        code: 'USER_DOES_NOT_EXIST',
      });
    }

    const [receivingUserInventory, sendingUserInventory] = await Promise.all([
      this.inventoryService.findMany({
        where: {
          items: {
            uuid: {
              in: receiving_user_items,
            },
          },
          user_id: receivingUser.id,
        },
      }),
      this.inventoryService.findMany({
        where: {
          items: {
            uuid: {
              in: sending_user_items,
            },
          },
          user_id: user.id,
        },
      }),
    ]);

    /**
     * Check if the users have all the items
     */

    if (
      receivingUserInventory.user_items.length !== receiving_user_items.length
    ) {
      throw new BadRequestException({
        message: 'The receiving user does not have all the items',
        code: 'USER_DOES_NOT_HAVE_ALL_ITEMS',
      });
    }

    if (sendingUserInventory.user_items.length !== sending_user_items.length) {
      throw new BadRequestException({
        message: 'The sending user does not have all the items',
        code: 'USER_DOES_NOT_HAVE_ALL_ITEMS',
      });
    }

    await this.tradeOffersService.create({
      users_trade_offers_offering_user_idTousers: {
        connect: {
          id: user.id,
        },
      },
      users_trade_offers_receiving_user_idTousers: {
        connect: {
          id: receivingUser.id,
        },
      },
      trade_offer_items: {
        createMany: {
          data: [
            ...receivingUserInventory.user_items.map((item) => ({
              item_id: item.id,
              owner_user_id: receivingUser.id,
            })),
            ...sendingUserInventory.user_items.map((item) => ({
              item_id: item.id,
              owner_user_id: user.id,
            })),
          ],
        },
      },
      status: TradeOfferStatus.PENDING,
    });

    return {
      message: 'Trade offer created successfully',
      data: {
        trade_offer: {
          receiving_user_uuid: receivingUser.uuid,
          sending_user_uuid: user.uuid,
          created_at: new Date(),
        },
      },
    };
  }
}
