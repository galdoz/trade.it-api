import { Module } from '@nestjs/common';
import { PrismaService } from '../../shared/services/prisma.service';
import { ItemsService } from './items.service';
import { ItemsController } from './items.controller';

@Module({
  providers: [PrismaService, ItemsService],
  controllers: [ItemsController],
  exports: [ItemsService],
})
export class ItemsModule {}
