import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../shared/services/prisma.service';
import { Prisma, items } from '@prisma/client';

@Injectable()
export class ItemsService {
  constructor(private readonly prisma: PrismaService) {}

  async create(data: Prisma.itemsCreateInput): Promise<items> {
    return this.prisma.items.create({ data });
  }

  async delete(where: Prisma.itemsWhereUniqueInput): Promise<items> {
    return this.prisma.items.delete({ where });
  }

  async update(params: {
    where: Prisma.itemsWhereUniqueInput;
    data: Prisma.itemsUpdateInput;
  }): Promise<items> {
    const { where, data } = params;
    return this.prisma.items.update({ data, where });
  }

  async findOne(where: Prisma.itemsWhereUniqueInput): Promise<items | null> {
    return this.prisma.items.findUnique({ where });
  }

  async findMany(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.itemsWhereUniqueInput;
    where?: Prisma.itemsWhereInput;
    orderBy?: Prisma.itemsOrderByWithAggregationInput;
  }): Promise<{ total: number; items: items[] }> {
    const { skip, take, cursor, where, orderBy } = params;

    const [total, items] = await this.prisma.$transaction([
      this.prisma.items.count(),
      this.prisma.items.findMany({
        skip,
        take,
        cursor,
        where,
        orderBy,
      }),
    ]);

    return {
      total,
      items,
    };
  }
}
