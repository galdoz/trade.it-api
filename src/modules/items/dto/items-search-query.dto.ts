import { ApiProperty } from '@nestjs/swagger';
import { SearchQueryDto } from '../../../shared/dto/search-query.dto';
import { Prisma } from '@prisma/client';
import { IsEnum, IsOptional } from 'class-validator';

export class ItemsSeachQueryDto extends SearchQueryDto {
  @ApiProperty({
    enum: Prisma.ItemsScalarFieldEnum,
    description: 'Sort by field',
    example: 'id',
    required: false,
  })
  @IsOptional()
  @IsEnum(Prisma.ItemsScalarFieldEnum, { each: true })
  orderBy?: string;
}
