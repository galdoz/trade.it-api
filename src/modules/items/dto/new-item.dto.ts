import { ApiProperty } from '@nestjs/swagger';
import { items } from '@prisma/client';
import { IsEnum, IsString, MaxLength, MinLength } from 'class-validator';
import { ItemType } from '../constants/item-type.constant';

export class NewItemDto
  implements Pick<items, 'title' | 'description' | 'type'>
{
  @ApiProperty({
    description: 'The title of the item',
    example: 'My Item',
  })
  @MinLength(3)
  @MaxLength(30)
  @IsString()
  title: string;

  @ApiProperty({
    description: 'The description of the item',
    example: 'My Item Description',
  })
  @MinLength(3)
  @MaxLength(60)
  description: string;

  @ApiProperty({
    description: 'The type of the item',
    example: ItemType.DIGITAL,
    enum: ItemType,
  })
  @IsEnum(ItemType)
  type: string;

  @ApiProperty({
    type: 'string',
    format: 'binary',
    description:
      'The image of the item | Max size: 10MB | Allowed types: png, jpeg, jpg',
    maximum: 1024 * 1024 * 10,
  })
  image: Express.Multer.File;
}
