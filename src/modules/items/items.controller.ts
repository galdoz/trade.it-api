import {
  BadRequestException,
  Body,
  Controller,
  FileTypeValidator,
  Get,
  MaxFileSizeValidator,
  ParseFilePipe,
  Post,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { AccessTokenGuard } from '../authorization/guards/access-token.guard';
import { ItemsSeachQueryDto } from './dto';
import { NewItemDto } from './dto/new-item.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { ItemsService } from './items.service';
import { ResponseFormat } from '../../shared/interceptors/response-format.interceptor';

@Controller('items')
@ApiTags('Items')
@ApiBearerAuth()
@UseGuards(AccessTokenGuard)
export class ItemsController {
  constructor(private readonly itemsService: ItemsService) {}
  @Get('')
  @ApiOperation({
    summary: 'Get all items',
    description: 'Get all items',
  })
  async findAll(
    @Query() query: ItemsSeachQueryDto,
  ): Promise<ResponseFormat<any>> {
    const { orderBy, skip, sortBy, take } = query;
    const result = await this.itemsService.findMany({
      skip,
      take,
      orderBy: { [orderBy]: sortBy },
    });
    if (result.items.length <= 0) {
      throw new BadRequestException('No items found');
    }
    return {
      message: 'All items',
      data: result,
    };
  }

  @Post('')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('image'))
  @ApiOperation({
    summary: 'Create a new item',
    description: 'Create a new item',
  })
  async create(
    @Body() body: NewItemDto,
    @UploadedFile(
      new ParseFilePipe({
        fileIsRequired: true,
        validators: [
          new MaxFileSizeValidator({ maxSize: 1024 * 1024 * 10 }),
          new FileTypeValidator({ fileType: '.(png|jpeg|jpg)' }),
        ],
      }),
    )
    file: Express.Multer.File,
  ): Promise<ResponseFormat<any>> {
    const { description, title, type } = body;
    const {} = file;
    const item = await this.itemsService.create({
      title,
      description,
      type,
    });
    return {
      message: 'Item created successfully',
      data: item,
    };
  }
}
