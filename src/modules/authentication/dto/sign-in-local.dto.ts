import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MinLength } from 'class-validator';

export class SignInLocalDto {
  @ApiProperty({
    example: 'my-email@email.com',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    example: 'MyAmazingPassword!',
  })
  @IsString()
  @MinLength(5)
  password: string;
}
