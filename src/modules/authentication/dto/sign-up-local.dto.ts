import { ApiProperty } from '@nestjs/swagger';
import { users } from '@prisma/client';
import { IsEmail, IsString, MinLength } from 'class-validator';

export class SignUpLocalDto
  implements Pick<users, 'first_name' | 'last_name' | 'email'>
{
  @ApiProperty({
    example: 'John',
    description: 'The first name of the user',
  })
  @IsString()
  first_name: string;

  @ApiProperty({
    example: 'Doe',
    description: 'The last name of the user',
  })
  @IsString()
  last_name: string;

  @ApiProperty({
    example: 'my-email@email.com',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    example: 'MyAmazingPassword!',
  })
  @IsString()
  @MinLength(5)
  password: string;
}
