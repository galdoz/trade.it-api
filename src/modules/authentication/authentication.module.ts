import { Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { EncrypterSevice } from '../../shared/services/encrypter.service';
import { LocalStrategy } from './strategies/local.strategy';
import { AuthenticationService } from './authentication.service';
import { PassportModule } from '@nestjs/passport';
import { AuthenticationController } from './authentication.controller';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';

@Module({
  providers: [EncrypterSevice, LocalStrategy, AuthenticationService],
  exports: [LocalStrategy, AuthenticationService],
  imports: [UsersModule, PassportModule, ConfigModule, JwtModule.register({})],
  controllers: [AuthenticationController],
})
export class AuthenticationModule {}
